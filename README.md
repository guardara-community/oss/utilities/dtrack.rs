# DTrack

Manage dependency vulnerabilities and license compliance in the CI using Dependency Track.

## Download

You can find the latest version (compiled for `amd64`) of Dtrack in the Project's [Package Registry](https://gitlab.com/guardara-community/oss/utilities/dtrack.rs/-/packages). Of course, you can always compile Dtrack yourself.

## Prerequisites

You should have [Dependency Track](https://dependencytrack.org) up and running. You will need an API key with the following permissions granted:

 * BOM_UPLOAD 
 * PORTFOLIO_MANAGEMENT
 * PROJECT_CREATION_UPLOAD 
 * VIEW_POLICY_VIOLATION
 * VIEW_PORTFOLIO
 * VIEW_VULNERABILITY

You can create API keys in Dependency Track on the *Access Management / Teams* page.

## Usage

```
Usage:
  dtrack [OPTIONS]

Manage dependency vulnerabilities and license compliance in the CI using
Dependency Track. Make sure you set the `DTRACK_URL` and `DTRACK_API_KEY`
environment variables.

Optional arguments:
  -h,--help             Show this help message and exit
  -p,--project PROJECT  Project name
  -v,--version VERSION  Project version
  -s,--sbom SBOM        Path to SBOM file (default: 'bom.json')
```

An example of how to use Dtrack is shown below. 

```
# See the Authentication section below for more information
export DTRACK_URL="https://sca.example.org:8000"
export DTRACK_API_KEY="my_secret_key"

dtrack -p example -v 1.0.0 -s sbom.json
```

If the path to the SBOM file is not provided (`-s`) it defaults to `bom.json`. You must configure  `cyclonedx-bom` to produce a JSON file.

Dtrack automatically creates the project in Dependency Track if it does not exist.

Dtrack will display vulnerabilities and license violations on the console. This is practicaly because the information is immediately available to developers, instead of having to manage access to the information in Dependency Tracker. For example, running Dtrack against [this BOM file](https://github.com/CycloneDX/bom-examples/blob/master/SBOM/cern-lhc-vdm-editor-e564943/bom.json) with the example Policy shown later on this page produces the output below. 

```
================================================================================
VULNERABLE DEPENDENCIES
================================================================================
NAME                           VERSION            SEVERITY   VULNERABILITY                 
--------------------------------------------------------------------------------
minimist                       0.0.8              CRITICAL   GHSA-xvch-5gv4-984h           
minimist                       0.0.8              MEDIUM     GHSA-vh95-rmgr-6w4m           
minimist                       0.0.8              MEDIUM     GHSA-7fhm-mqm4-2wp7           
minimatch                      3.0.4              HIGH       GHSA-f8q6-p94x-37v3           
ws                             6.2.1              MEDIUM     GHSA-6fc8-4gx4-v693           
minimist                       0.0.8              MEDIUM     CVE-2020-7598                 
minimatch                      3.0.4              HIGH       sonatype-2021-4879            
ws                             6.2.1              MEDIUM     CVE-2021-32640                

================================================================================
VULNERABILITY SUMMARY
================================================================================
SEVERITY        COUNT               
--------------------------------------------------------------------------------
CRITICAL        1
HIGH            2
MEDIUM          5
LOW             0

================================================================================
LICENSE POLICY VIOLATIONS
================================================================================
NAME                                VERSION            LICENSE                       
--------------------------------------------------------------------------------
minimatch                           3.0.4              ISC                         
wrappy                              1.0.2              ISC                         
once                                1.4.0              ISC                         
inflight                            1.0.6              ISC                         
fs.realpath                         1.0.0              ISC                         
glob                                7.1.4              ISC                         
rimraf                              2.7.1              ISC                         
inherits                            2.0.4              ISC                         
extract-zip                         1.6.7              BSD-2-Clause
```

## Authentication

You must set the `DTRACK_URL` and `DTRACK_API_KEY` environment variables so Dtrack can authenticate to Dependency Track. The reason behind not exposing these as command line arguments are:

 * The API key (`DTRACK_API_KEY`) must be kept a secret. The best way to do it is by setting it as an environment variable so it's value is not exposed in logs. Both [GitLab](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-group) and GitHub provides a way to set environment variables safely and you should seriously consider setting up `DTRACK_API_KEY` that way. In other way: **do not** set `DTRACK_API_KEY` in GitLab-ci yaml, GitHub actions, Dockerfiles, etc.
 * The Dependency Track URL (`DTRACK_URL`) is "hidden" similarly to the API key to prevent/reduce the likelihood of potential abuse of your Dependency Track deployment.

A good example is the `gitlab-ci.yml` file of this project. You cannot find the Dependency Track URL or API key in the document as it was configured as a CI/CD variable for the project.

## Dtrack Policy

The Dtrack policy file allows defining rules that govern how software vulnerabilities and licenses are handled.

### Custom Policy Engine

Dtrack implements a custom policy management system that operates independently of the Policy Management feature provided by Dependency Track. There are three reasons behind this implementation:

 1. Dtrack provides an easier and more versatile vulnerability policy to be implemented
 2. Dtrack addresses some of the limitations of Dependency Track's license auditing feature 
 3. Dtrack's implementation allows sharing your policy with the public easily  

### Policy Location Options

The policy file lookup works like this:

 1. If the value of the `DTRACK_POLICY` environment variable starts with `https://`, Dtrack will always try to load the policy file from the given URL. If the value does not start with `https://`, Dtrack will try to load the file from the disk, in which case `DTRACK_POLICY` should be the full path to the file, .e.g.: `/tmp/mypolicy.yml`.
 2. If the `DTRACK_POLICY` environment variable is not set, Dtrack will try to load the policy file from the current working directory. The name of the policy file must be `.dtrack-policy.yml`.

If the policy file does not exists, Dtrack will silently upload the BOM file to Dependency Track and exit without saying a word.

### Policy Options

The policy file is a YAML file that allows defining rules that control how vulnerabilities are handled (`vulnerability_rules` below) and a license policy (`licensing` below).

```
version: 1.0
vulnerability_rules:
  - rule:
    severity: critical
    max: 1
    fail: true
  - rule:
    severity: high
    max: 2
    fail: true
  - rule:
    severity: medium
    max: 10
    fail: true
  - rule:
    severity: low
    max: 10
    fail: false
licensing:
  permitted:
    - MIT
    - Apache-2.0
  ignore:
    - component:
      name: my_component
      version: 0.1.0

```

Both the `vulnerability_rules` and `licensing` sections are optional. For example, you can turn of the license validation feature by simply removing the entire `licensing` section. The same goes for the vulnerability rules.

#### Vulnerability Rules

The `vulnerability_rules` section is a list of rules with the following properties:

| Property | Description |
| -------- | ----------- |
| severity | Either `critical`, `high`, `medium` or `low`. |
| max      | If the number of vulnerabilities for the `severity` is greater or equal `max`, action it. The action taken is determined by the value of the `fail` property. |
| fail     | `true` if the test is to be failed, `false` otherwise. |

#### License Policy

The licensing policy has two main sections, `permitted` and `ignore` as shown below.

```
licensing:
  permitted:
    - MIT
    - Apache-2.0
  ignore:
    - component:
      name: my_component
      version: 0.1.0
```

The `permitted` section allows defining the permitted licenses. Of course, the preferred is to use a list of SPDX license IDs/expressions, however, in certain cases, for example, you may have to provide the full name of the license.

The `ignore` section allows excluding components from the license check based on their name and version number. Please note that if you wish to ignore a component, you must specify both the `name` and `version` number.

### Policy Violations

Dtrack returns with error code 1 (fails the test) in case any Policy Violations detected.
