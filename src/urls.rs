use crate::config::Config;
use crate::models::dependencytrack::bom::BomToken;

pub fn get_lookup_project_url(config: &Config) -> String {
    format!(
        "{}/api/v1/project/lookup?&name={}&version={}&excludeInactive=false",
        config.dtrack_url, config.project.name, config.project.version
    )
}

pub fn get_create_project_url(config: &Config) -> String {
    format!("{}/api/v1/project", config.dtrack_url)
}

pub fn get_bom_upload_url(config: &Config) -> String {
    format!("{}/api/v1/bom", config.dtrack_url)
}

pub fn get_vulnerabilities_url(config: &Config) -> String {
    format!(
        "{}/api/v1/finding/project/{}",
        config.dtrack_url,
        &config.project.uuid.clone().unwrap()
    )
}

pub fn get_bom_token_url(config: &Config, token: &BomToken) -> String {
    format!("{}/api/v1/bom/token/{}", config.dtrack_url, token.token)
}
