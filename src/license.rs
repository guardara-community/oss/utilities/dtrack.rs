use crate::models::bom::component::{Component, LicenseVariant};

use crate::config::Config;
use crate::models::bom::Bom;
use crate::models::policy::IgnoredComponent;
use crate::utils::print_header;

// ----------------------------------------------------------------------------
// IMPL
// ----------------------------------------------------------------------------

/// Remove `(` and `)` from the beginning and end of a license expression.
///
/// The most recent version (2.3) of SPDF specifies the format of the
/// license expression with ABNF. [1]
///
/// If I interpreted things right, a compound expression such as
/// `MIT OR Apache-2.0` is just as valid as the old format
/// `(MIT OR Apache-2.0)`, which was kept for backward compatibility.
///
/// This method simply removes the parentheses. This way we work with
/// both the old compound expression format (still used by `package.json`[2])
/// and the new one.
///
/// [1] https://spdx.github.io/spdx-spec/v2.3/SPDX-license-expressions/
/// [2] https://docs.npmjs.com/cli/v8/configuring-npm/package-json
///
fn normalize_license_expression(expression: &str) -> String {
    let normalized = expression.trim_start_matches('(');
    let normalized = normalized.trim_end_matches(')');
    String::from(normalized)
}

// https://spdx.github.io/spdx-spec/using-SPDX-short-identifiers-in-source-files/
fn is_approved_license(permitted: &[String], value: String) -> bool {
    let value = normalize_license_expression(&value);

    if value.contains(" OR ") && value.contains(" AND ") {
        panic!("Hoh, you better get a lawyer! License expression contains both OR and AND operations. This is currently not supported.");
    }

    if !value.contains(" OR ") && !value.contains(" AND ") {
        return permitted.contains(&value);
    }

    // Check if the permitted licenses contain at least one of the licenses
    if value.contains(" OR ") {
        let licenses = value.split(" OR ");
        for license in licenses {
            if permitted.contains(&String::from(license)) {
                return true;
            }
        }
    }

    // Check all the licenses are in the permitted list
    if value.contains(" AND ") {
        let licenses = value.split(" AND ");
        let mut license_count: usize = 0;
        let mut included: usize = 0;
        for license in licenses {
            if permitted.contains(&String::from(license)) {
                included += 1;
            }
            license_count += 1;
        }
        return included == license_count;
    }

    false
}

fn get_license_string(license: &LicenseVariant) -> String {
    match license {
        LicenseVariant::License { license: l } => l.id.clone(),
        LicenseVariant::Expression { expression: e } => e.clone(),
        LicenseVariant::LicenseName { license: n } => n.name.clone(),
    }
}

pub fn get_all_license_string(component: &Component) -> String {
    let licenses = component.licenses.as_ref();
    if licenses.is_none() {
        return String::from("N/A");
    }
    let licenses = licenses.unwrap();
    let mut license_list: Vec<String> = Vec::new();
    for license in licenses {
        license_list.insert(0, get_license_string(license));
    }
    license_list.join(", ")
}

fn process_component_licenses(permitted: &[String], licenses: &Vec<LicenseVariant>) -> bool {
    let mut license_list: Vec<String> = Vec::new();
    for license in licenses {
        license_list.insert(0, get_license_string(license));
    }
    for license in license_list {
        if is_approved_license(permitted, license) {
            continue;
        }
        return false;
    }
    true
}

fn is_component_ignored(
    ignore_list: Option<&Vec<IgnoredComponent>>,
    component: &Component,
) -> bool {
    if ignore_list.is_none() {
        return false;
    }
    let ignored = ignore_list.unwrap();
    let result = ignored
        .iter()
        .find(|c| c.name == component.name && c.version == component.version);
    result.is_some()
}

// TODO: write tests and test the whole thing!!!
pub fn is_license_permitted(config: &Config, bom: &Bom) -> bool {
    if bom.components.is_empty() {
        return true;
    }
    if config.policy.is_none() {
        return true;
    }
    let policy = config.policy.as_ref().unwrap();
    if policy.licensing.is_none() {
        return true;
    }
    let licensing = policy.licensing.as_ref().unwrap();
    let permitted = licensing.permitted.as_ref();
    let ignore = licensing.ignore.as_ref();

    if permitted.is_none() {
        return true;
    }
    let permitted = permitted.unwrap();
    if permitted.is_empty() {
        return true;
    }

    let mut forbidden: Vec<&Component> = Vec::new();

    for component in bom.components.iter() {
        if is_component_ignored(ignore, component) {
            continue;
        }

        if component.licenses.is_none() {
            println!(
                "[WARN] Could not find a license definition for component '{}:{}'.",
                component.name, component.version
            );
            continue;
        }
        let licenses = component.licenses.as_ref().unwrap();
        let approved = process_component_licenses(&permitted, licenses);
        if approved {
            continue;
        }
        forbidden.insert(0, component);
    }

    if forbidden.is_empty() {
        return true;
    }

    print_header("LICENSE POLICY VIOLATIONS");
    println!("{:<35} {:<18} {:<30}", "NAME", "VERSION", "LICENSE");
    println!("{}", "-".repeat(80));

    for component in forbidden {
        println!(
            "{:<35} {:<18} {:<28}",
            component.name,
            component.version,
            get_all_license_string(component),
        );
    }

    false
}

// ----------------------------------------------------------------------------
// TESTS
// ----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::{
        config::{Config, Options},
        models::{
            self,
            bom::{
                component::{Component, License},
                Bom,
            },
            policy::{IgnoredComponent, Licensing, Policy},
            project::Project,
        },
    };

    fn get_test_project() -> Project {
        Project {
            name: "project_name".to_string(),
            version: "project_version".to_string(),
            active: true,
            classifier: Some("classifier".to_string()),
            uuid: Some("660bc21b-73ab-47d9-9e24-c4a824a6ff7f".to_string()),
            lastBomImport: Some(0 as usize),
            lastBomImportFormat: Some("lastBomImportFormat".to_string()),
            lastInheritedRiskScore: Some(0 as f64),
        }
    }

    fn get_test_config_with_policy(policy: Option<Policy>) -> Config {
        Config {
            dtrack_url: "https://unittest".to_string(),
            dtrack_key: "fake_key".to_string(),
            options: Options {
                sbom_file: "unittest".to_string(),
            },
            project: get_test_project(),
            policy,
        }
    }

    fn get_test_bom_with_components(components: Vec<Component>) -> Bom {
        Bom {
            bomFormat: "unittest".to_string(),
            specVersion: "1.0".to_string(),
            components: components,
        }
    }

    #[test]
    fn is_license_permitted_permitted() {
        let components = vec![Component {
            r#type: "library".to_string(),
            name: "microservice".to_string(),
            version: "0.1.0".to_string(),
            purl: "none".to_string(),
            licenses: Some(vec![models::bom::component::LicenseVariant::License {
                license: License {
                    id: "MIT".to_string(),
                    url: Some("".to_string()),
                },
            }]),
        }];

        let bom = get_test_bom_with_components(components);

        let policy = Some(Policy {
            version: "1".to_string(),
            vulnerability_rules: None,
            licensing: Some(Licensing {
                permitted: Some(vec!["MIT".to_string()]),
                ignore: None,
            }),
        });
        let config = get_test_config_with_policy(policy);

        let result = super::is_license_permitted(&config, &bom);
        assert_eq!(result, true);
    }

    #[test]
    fn is_license_permitted_not() {
        let components = vec![Component {
            r#type: "library".to_string(),
            name: "microservice".to_string(),
            version: "0.1.0".to_string(),
            purl: "none".to_string(),
            licenses: Some(vec![models::bom::component::LicenseVariant::License {
                license: License {
                    id: "GPLv3".to_string(),
                    url: Some("".to_string()),
                },
            }]),
        }];

        let bom = get_test_bom_with_components(components);

        let policy = Some(Policy {
            version: "1".to_string(),
            vulnerability_rules: None,
            licensing: Some(Licensing {
                permitted: Some(vec!["MIT".to_string()]),
                ignore: None,
            }),
        });
        let config = get_test_config_with_policy(policy);

        let result = super::is_license_permitted(&config, &bom);
        assert_eq!(result, false);
    }

    #[test]
    fn is_license_permitted_component_ignored() {
        let components = vec![Component {
            r#type: "library".to_string(),
            name: "microservice".to_string(),
            version: "0.1.0".to_string(),
            purl: "none".to_string(),
            licenses: Some(vec![models::bom::component::LicenseVariant::License {
                license: License {
                    id: "GPLv3".to_string(),
                    url: Some("".to_string()),
                },
            }]),
        }];

        let bom = get_test_bom_with_components(components);

        let policy = Some(Policy {
            version: "1".to_string(),
            vulnerability_rules: None,
            licensing: Some(Licensing {
                permitted: Some(vec!["MIT".to_string()]),
                ignore: Some(vec![IgnoredComponent {
                    name: "microservice".to_string(),
                    version: "0.1.0".to_string(),
                }]),
            }),
        });
        let config = get_test_config_with_policy(policy);

        let result = super::is_license_permitted(&config, &bom);
        assert_eq!(result, true);
    }
}
