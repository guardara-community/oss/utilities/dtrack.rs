use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct BomToken {
    pub token: String,
}

#[derive(Deserialize, Serialize)]
pub struct BomStatus {
    pub processing: bool,
}
