use serde::{Deserialize, Serialize};

#[allow(non_snake_case)]
#[derive(Deserialize, Serialize, Debug)]
pub struct Component {
    pub r#type: String,
    pub name: String,
    pub version: String,
    pub purl: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub licenses: Option<Vec<LicenseVariant>>,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(untagged)]
pub enum LicenseVariant {
    License { license: License },
    Expression { expression: String },
    LicenseName { license: LicenseName },
}

#[derive(Deserialize, Serialize, Debug)]
pub struct License {
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct LicenseName {
    pub name: String,
}
