use std::collections::HashMap;

use crate::config::Config;
use crate::models::dependencytrack::vulnerability::Vulnerabilities;
use crate::utils::print_header;

pub struct SeverityCount(HashMap<String, usize>);

impl SeverityCount {
    pub fn new(vulnerabilities: &Vulnerabilities) -> Self {
        let mut severity_count: HashMap<String, usize> = HashMap::new();
        severity_count.insert(String::from("CRITICAL"), 0);
        severity_count.insert(String::from("HIGH"), 0);
        severity_count.insert(String::from("MEDIUM"), 0);
        severity_count.insert(String::from("LOW"), 0);
        let mut severity_count = SeverityCount(severity_count);

        for vulnerability in vulnerabilities.0.iter() {
            severity_count.increment(&vulnerability.vulnerability.severity);
        }
        severity_count
    }

    fn increment(&mut self, severity: &String) {
        if !self.0.contains_key(severity) {
            self.0.insert(severity.clone(), 0);
            return;
        }
        let value = self.0.get(severity).unwrap();
        self.0.insert(severity.clone(), value + 1);
    }

    pub fn print(&self) {
        print_header("VULNERABILITY SUMMARY");
        println!("{:<15} {:<20}", "SEVERITY", "COUNT");
        println!("{}", "-".repeat(80));

        for severity in ["CRITICAL", "HIGH", "MEDIUM", "LOW"] {
            println!("{:<15} {}", &severity, self.0.get(severity).unwrap());
        }
    }
}

pub fn should_fail_test(config: &Config, severity_count: &SeverityCount) -> bool {
    if config.policy.is_none() {
        return false;
    }
    let policy = config.policy.as_ref().unwrap();
    if policy.vulnerability_rules.is_none() {
        return false;
    }
    let rules = policy.vulnerability_rules.as_ref().unwrap();
    if rules.is_empty() {
        return false;
    }

    for rule in rules {
        let vuln_count = match severity_count.0.get(&rule.severity.to_uppercase()) {
            Some(value) => value,
            None => continue,
        };
        if vuln_count >= &rule.max && rule.fail {
            return true;
        }
    }
    false
}
